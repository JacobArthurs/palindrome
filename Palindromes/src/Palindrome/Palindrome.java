package Palindrome;

public class Palindrome {
	public static void main (String args[]) {
		System.out.println("Is anna a palindrome?");
		System.out.println(isPalindrome("anna"));
	}
	static boolean isPalindrome (String s) {
		s = s.toLowerCase().replaceAll(" ","");
		for(int i=0,j=s.length()-1;i<j;i++,j--){
			if(s.charAt(i) != s.charAt(j)){
				return false;
			}
		}
		return true;
	}
}
