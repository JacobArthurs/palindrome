package Palindrome;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsPalindromeStandard() {
		assertTrue("Should be true",Palindrome.isPalindrome("anna"));
	}
	
	@Test
	public void testIsPalindromeNegative() {
		assertTrue("Should be false",!Palindrome.isPalindrome("evfsa"));
	}
	
	@Test
	public void testIsPalindromeBoundaryUp() {
		assertTrue("Should be false",!Palindrome.isPalindrome("annnb"));
	}
	
	@Test
	public void testIsPalindromeBoundaryLow() {
		assertTrue("Should be false",!Palindrome.isPalindrome("bnnna"));
	}

}
